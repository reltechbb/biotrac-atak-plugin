/*
 * BioTracMetric.java
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * Holds a single BioTrac metric
 *
 */
package com.atakmap.android.plugintemplate;

import gov.tak.api.annotation.Nullable;

/**
 * Holder for a single reading metric
 */
public class BioTracMetric {
        // name of the metric - e.g. exertion - mandatory
        private String metricName;
        // the value of the metric - not all metrics use this field
        private Double metricValue = null;
        // alert threshold - not all metrics use this field
        private Double metricThreshold;
        // current alert level (green, yellow, red) for this metric
        // not all metrics use this field
        private BioTracAlertLevelEnum metricLevel;

        public BioTracMetric (String name, @Nullable String value, @Nullable String threshold,
                              @Nullable String level) {
            setName (name);
            if (value != null) {
                setValue(value);
            }
            if (threshold != null) {
                setThreshold(threshold);
            }
            if (level != null) {
                setAlertLevel(level);
            }
        }

        public void setName (String name) {
            metricName = name;
        }
        public String getName () {
            return metricName;
        }
        public void setValue (String value) {
            metricValue = Double.parseDouble(value);
        }
        public Double getValue () {
            return metricValue;
        }
        public void setThreshold (String threshold) {
            if (threshold.length() > 0) {
                metricThreshold = Double.parseDouble(threshold);
            } else {
                metricThreshold = 0.0;
            }
        }
        public Double getThreshold () {
            return metricThreshold;
        }
        public void setAlertLevel (String level) {
            metricLevel = BioTracAlertLevelEnum.valueOf(level);
        }
        public BioTracAlertLevelEnum getAlertLevel () {
            return metricLevel;
        }
        public String getAlertLevelString () {
            return metricLevel.toString();
        }
}
