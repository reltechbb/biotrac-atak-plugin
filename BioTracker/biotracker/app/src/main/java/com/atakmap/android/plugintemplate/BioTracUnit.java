/*
 * BioTracUnit.java
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * An individual BioTrac unit (wrist band)
 *
 */
package com.atakmap.android.plugintemplate;

/**
 * An individual BioTrac unit (wrist bane)
 */
public class BioTracUnit {
    private String uid = null;
    private String id = null;
    private String name = "no name";
    private BioTracAlertLevelEnum overallAlertLevel = BioTracAlertLevelEnum.GREEN;
    private BioTracReading lastReading = null;
    // this item has been deleted
    private Boolean deleted = false;

    public BioTracUnit (String uid, String name){
        this.uid = uid;
        this.name = name;
        this.id = uid.replace("BIOTRAC-", "");
    };
    public String getUID () {
        return uid;
    }
    public String getID () {
        return id;
    }
    public String getName () {
        return name;
    }
    public void setReading (BioTracReading reading) {
        lastReading = reading;
    }
    public BioTracReading getLastReading () {
        return lastReading;
    }
    public void setOverallAlertLevel (BioTracAlertLevelEnum level) {
        overallAlertLevel = level;
    }
    public BioTracAlertLevelEnum getOverallAlertLevel () {
        return overallAlertLevel;
    }
    public void setDeleted (Boolean deleted) { this.deleted = true; }
    public Boolean isDeleted () { return deleted; }
}
