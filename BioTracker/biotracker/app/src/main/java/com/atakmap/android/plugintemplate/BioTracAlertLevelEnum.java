/*
 * BioTracAlertLevelEnum.java
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * Enums for the alert levels
 *
 */
package com.atakmap.android.plugintemplate;

/**
 * Enums for the alert levels
 * @Author jmerritt
 */
public enum BioTracAlertLevelEnum {
    // green - no alert
    // yellow - caution
    // red - alert
    // gray - stale
    GREEN, YELLOW, RED, GRAY
}
