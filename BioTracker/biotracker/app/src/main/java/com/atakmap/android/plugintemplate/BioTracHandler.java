/*
 * <put file name here>
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * <put description here>
 *
 */
package com.atakmap.android.plugintemplate;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.atakmap.android.maps.MapEvent;
import com.atakmap.android.maps.MapEventDispatcher;
import com.atakmap.android.maps.MapGroup;
import com.atakmap.android.maps.MapItem;
import com.atakmap.android.maps.MapView;
import com.atakmap.android.maps.Marker;
import com.atakmap.android.menu.PluginMenuParser;
import com.atakmap.android.plugintemplate.plugin.R;
import com.atakmap.coremap.cot.event.CotDetail;
import com.atakmap.coremap.cot.event.CotEvent;
import com.atakmap.coremap.maps.assets.Icon;
import com.atakmap.coremap.maps.coords.GeoPoint;
import com.atakmap.coremap.maps.time.CoordinatedTime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.atakmap.android.maps.MapView.getMapView;
import static com.atakmap.android.plugintemplate.BioTracAlertLevelEnum.*;

public class BioTracHandler implements Runnable {
    private final Thread thread;
    private final CoordinatedTime staleTime = new CoordinatedTime(60000);
    public static final String TAG = "BioTrac";
    private static final String BIOTRAC_TYPE = "y-f-G-U-C";
    private static final ArrayList<BioTracUnit> bioTracUnitList = new ArrayList<>();
    private final Context pluginContext;
    private final BioTracMenuHandler menuHandler;
    //private View biotracView;

    /**
     * constructor
     * @param context - the context
     * @param biotracView - the view
     */
    public BioTracHandler(Context context, View biotracView) {
        MapView mapView = getMapView();
        if (mapView.getRootGroup().findMapGroup(
                TAG) == null) {
            mapView.getRootGroup().addGroup("TAG");
            Log.d(TAG, "Creating Map Group: " + TAG);
            // todo figure out why we can't find this group
        } else {
            Log.d(TAG, "Map Group: " + TAG + " already exists");
        }
        pluginContext = context;
        menuHandler = new BioTracMenuHandler(biotracView, this);
        thread = new Thread(this);
        thread.start();

        Log.i(TAG, "BioTrac handler started");
    }

    /**
     * process a COT event
     * @param event - the COT event
     */
    public void processEvent(CotEvent event) {
        Log.d(TAG, "ProcessEvent:Type: " + event.getType());
        BioTracReading newReading = null;
        // discard anything received with wrong type
        if (event.getType().equals(BIOTRAC_TYPE)) {
            String uid = event.getUID();

            CotDetail detail = event.getDetail();
            List<CotDetail> detailList = detail.getChildren();
            Log.d (TAG, "DetailList has " + detailList.size() + " children");
            // detailList has zero or more readings
            for (CotDetail reading : detailList) {
                List<CotDetail> readingList = reading.getChildren();
                // readingList has a child for each time, name metric, etc.
                Log.d (TAG, "ReadingList has " + readingList.size() + " children");
                newReading = new BioTracReading();
                for (CotDetail readingItem : readingList) {
                    newReading.setUID(uid);
                    switch (readingItem.getElementName()) {
                        case "time":
                            newReading.setTime(readingItem.getInnerText());
                            //Log.d (TAG, "Setting time");
                            break;
                        case "name":
                            newReading.setName(readingItem.getInnerText());
                            //Log.d (TAG, "Setting name: " + newReading.getName());
                            break;
                        case "metric":
                            parseMetric(newReading, readingItem);
                            break;
                        case "point":
                            newReading.setGeoPoint(
                                    new GeoPoint(
                                            Double.parseDouble(readingItem.getAttribute("lat")),
                                            Double.parseDouble(readingItem.getAttribute("lon")),
                                            Double.parseDouble(readingItem.getAttribute("hae")),
                                            Double.parseDouble(readingItem.getAttribute("ce")),
                                            Double.parseDouble(readingItem.getAttribute("le"))));
                            break;
                        default:
                            Log.e(TAG, "Unexpected element name: " + readingItem.getElementName());
                            break;
                    }
                }
            }
            // get the unit, creating it if necessary
            BioTracUnit unit = getUnit(uid, newReading);
            // if the new reading doesn't have location data use the old one if possible
            if (newReading != null && newReading.getGeoPoint() == null && unit.getLastReading() != null) {
                newReading.setGeoPoint(unit.getLastReading().getGeoPoint());
            }
            unit.setReading(newReading);
            BioTracAlertLevelEnum level = GREEN;
            if (newReading != null) {
                level = determineOverallAlertLevel(newReading);
            }
            unit.setOverallAlertLevel(level);

            // display the icon if we have location data
            if (newReading != null && newReading.getGeoPoint() != null) {
                displayEvent(newReading, level);
            } else {
                // todo do something here
            }
        }
    }

    /**
     * Called when the plugin is removed or replaced to
     * free anything you "add"ed
     */
    public void dispose() {
        endStaleCheck = true;
        MapView mapView = getMapView();
        MapGroup mapGroup = mapView.getRootGroup().findMapGroup(
                "Drawing Objects");
        if (mapGroup == null) {
            Log.e(TAG, "dispose: Map group is null");
            return;
        }
        // remove any existing map icons
        Log.d(TAG, "removing existing icons");
        for (BioTracUnit unit : bioTracUnitList) {
            String uid = unit.getUID();
            // find existing map item
            MapItem item = mapGroup.deepFindUID(uid);
            // if item exists, remove it
            if (item != null) {
                Log.d(TAG, "Dispose removing " + uid);
                mapGroup.removeItem(item);
            } else {
                Log.d(TAG, "couldn't find " + uid);
            }
        }
        menuHandler.dispose();
        // kill the thread
        //thread.interrupt();
    }

    /**
     * parses items in the COT message with a "metric" tag
     * @param reading - the processed reading
     * @param item - the raw reading
     */
    private void parseMetric(BioTracReading reading, CotDetail item) {
        String name = item.getAttribute("name");
        String value = item.getAttribute("value");
        String threshold = item.getAttribute("threshold");
        String level = item.getAttribute("level");
        //Log.d (TAG, "Parsing metric: " + name + " " + value + " " + threshold + " " + level);
        switch (name) {
            case "exertion":
                reading.setExertion(value, threshold, level);
                break;
            case "heartrate":
                reading.setHeartRate(value, threshold, level);
                break;
            case "coretemp":
                reading.setCoreTemp(value, threshold, level);
                break;
            case "cadence":
                reading.setCadence(value);
                break;
            case "hssi":
                reading.setHSSI(value);
                break;
            case "hrlimitvalue":
                reading.setHRLimitValue(value, threshold, level);
                break;
            case "caloricburnrate":
                reading.setCaloricBurnRate(value);
                break;
            case "stateofcharge":
                reading.setStateOfCharge(value, level);
                break;
            case "fall":
                reading.setFall(level);
                break;
            case "tap":
                reading.setTap(level);
                break;
            case "nomovement":
                reading.setNoMovement(level);
                break;
            case "heartrateminusage":
                reading.setHeartRateMinusAge(level);
                break;
            default:
                Log.w(TAG, "Unknown metric name: " + name);
                break;
        }
    }

    private BioTracAlertDialog dialog = new BioTracAlertDialog();
    /**
     * displays the event on the map
     *
     * @param reading - the processed reading
     * @param level - the alert level
     */
    private void displayEvent(BioTracReading reading, BioTracAlertLevelEnum level) {
        Boolean newMarker;
        MapView mapView = getMapView();
        MapGroup mapGroup = mapView.getRootGroup().findMapGroup(
//                TAG);
                "Drawing Objects");
        if (mapGroup == null) {
            Log.e(TAG, "Map group is null");
            return;
        }
        // find existing map item
        //Log.d(TAG, "displayEvent: " + reading.getUID());
        MapItem item = mapGroup.deepFindUID(reading.getUID());
        // controls which icons are on top - lowest first
        // this has no effect
        //Double zOrder = 10.0;
        Marker marker;
        if (item == null) {
            // create a new marker since one does not exist
            GeoPoint point = reading.getGeoPoint();
            if (point != null) {
                marker = new Marker(reading.getGeoPoint(), reading.getUID());
            } else {
                marker = new Marker(reading.getUID());
                Log.w (TAG, "Creating new Marker without geopoint (it was null");
            }
            newMarker = true;
        } else {
            // update the existing marker
            marker = (Marker) item;
            newMarker = false;
        }
        // build the new map item
        // !! GeoPoint cannot be null
        //Marker marker = new Marker(reading.getGeoPoint(), reading.getUID());
        Icon.Builder iBuilder = null;
        Boolean markerVisible = true;
        //Log.d(TAG, "AlertLevel: " + level.toString());
        switch (level) {
            case GREEN:
                iBuilder = new Icon.Builder().setImageUri(0,
                        "android.resource://" + pluginContext.getPackageName()
                                + "/" + R.drawable.ss_icon_green);
                markerVisible = greenIsVisible;
                //zOrder = 20.0;
                break;
            case YELLOW:
                iBuilder = new Icon.Builder().setImageUri(0,
                        "android.resource://" + pluginContext.getPackageName()
                                + "/" + R.drawable.ss_icon_yellow);
                markerVisible = yellowIsVisible;
                //zOrder = 30.0;
                break;
            case RED:
                iBuilder = new Icon.Builder().setImageUri(0,
                        "android.resource://" + pluginContext.getPackageName()
                                + "/" + R.drawable.ss_icon_red);
                alertTone();
                markerVisible = redIsVisible;
                //zOrder = 40.0;
                //BioTracAlertDialog dialog = new BioTracAlertDialog();
                // doesn't work, generates exception
                //dialog.alertDialog(pluginContext, "Some text");
                break;
            case GRAY:
                iBuilder = new Icon.Builder().setImageUri(0,
                        "android.resource://" + pluginContext.getPackageName()
                                + "/" + R.drawable.ss_icon_gray);
                markerVisible = grayIsVisible;
                //zOrder = 10.0;
                //alertDialog ("Some text");
                break;
        }
        marker.setPoint(reading.getGeoPoint());
        marker.setIcon(iBuilder.build());
        // set if this marker should be visible
        marker.setVisible(markerVisible);
        marker.setMetaString("reading", buildMetadata(reading));

        //marker.setZOrder(zOrder);
        // this is a new marker so have to add additional things
        if (newMarker) {
            // make the marker removable
            marker.setMetaBoolean("removable", true);
            // make the marker clickable
            marker.setClickable(true);
            //marker.setAlwaysShowText(true);
            // set a custom radial menu
            marker.setMetaString("menu", getBiotracRadialMenu());

            MapEventDispatcher dispatcher = mapView.getMapEventDispatcher();
            dispatcher.addMapItemEventListener(marker,
                    new BioTracOnMapEventListener());
            // lets you know when items deleted
            marker.addOnGroupChangedListener(new GCListener());
            mapGroup.addItem(marker);
        }
    }

    /**
     * generates a tone if not muted
     */
    private void alertTone() {
        //Log.d(TAG, "Audible Alert Enabled: " + menuHandler.isAudibleEnabled());
        if (menuHandler.isAudibleEnabled()) {
            //Log.d(TAG, "Audible Alert Enabled2: " + menuHandler.isAudibleEnabled());
            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
            toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
        }
    }

    /**
     * builds the metadata to display when icon is tapped
     * @param reading - the reading to build the metadata string from
     * @return - the metadata string
     */
    private String buildMetadata(BioTracReading reading) {
        StringBuilder builder = new StringBuilder();
        // html doesn't work in these strings
        builder.append(reading.getName()).append("\n");
        builder.append("ID ").append(reading.getID());
        BioTracMetric metric = reading.getExertion();
        if (metric != null) {
            highlightMetadata (builder, metric);
            builder.append("Exertion ").append(metric.getValue()).append("%");
        }
        metric = reading.getHeartRate();
        if (metric != null) {
            highlightMetadata (builder, metric);
            builder.append("Heartrate ").append(metric.getValue()).append(" BPM");
        }
        metric = reading.getCoreTemp();
        if (metric != null) {
            highlightMetadata (builder, metric);
            builder.append("Core Temp ").append(metric.getValue()).append("\u00B0C");
        }
        metric = reading.getCadence();
        if (metric != null) {
            builder.append ("\n");
            builder.append("Cadence ").append(metric.getValue()).append(" Steps/Min");
        }
        metric = reading.getHSSI();
        if (metric != null) {
            builder.append ("\n");
            builder.append("HSSI ").append(metric.getValue()).append("%");
        }
        metric = reading.getHRLimitValue();
        if (metric != null) {
            highlightMetadata (builder, metric);
            builder.append("HRLV ").append(metric.getValue()).append(" BPM");
        }
        metric = reading.getCaloricBurnRate();
        if (metric != null) {
            builder.append ("\n");
            builder.append("CBR ").append(metric.getValue()).append(" Cal/Hour");
        }
        metric = reading.getStateOfCharge();
        if (metric != null) {
            builder.append ("\n");
            if (metric.getAlertLevel() == YELLOW) {
                builder.append ("> ");
            } else if (metric.getAlertLevel() == RED) {
                builder.append (">> ");
            }
            builder.append("Charge ").append(metric.getValue()).append("%");
        }
        metric = reading.getFall();
        if (metric != null) {
            highlightMetadata (builder, metric);
            builder.append("Fall");
        }
        metric = reading.getTap();
        if (metric != null) {
            highlightMetadata (builder, metric);
            builder.append("Tap");
        }
        metric = reading.getNoMovement();
        if (metric != null) {
            highlightMetadata (builder, metric);
            builder.append("No Movement");
        }
        metric = reading.getHeartRateMinusAge();
        if (metric != null) {
            highlightMetadata (builder, metric);
            builder.append("Heart Rate Minus Age");
        }
        //Log.d(TAG, "Building metadata: " + builder.toString());
        return builder.toString();
    }

    /**
     * Adds a linefeed (\n) and ">" if yellow and ">>" if red
     */
    private void highlightMetadata (StringBuilder builder, BioTracMetric metric) {
        builder.append ("\n");
        if (metric.getAlertLevel() == YELLOW) {
            builder.append("> ");
        } else if (metric.getAlertLevel() == RED) {
            builder.append(">> ");
        }
    }
    /**
     * finds an existing unit
     * creates one if necessary
     * @param uid - uid of the unit
     * @param reading - current reading
     * @return - the existing or new unit
     */
    private BioTracUnit getUnit(String uid, BioTracReading reading) {
        BioTracUnit foundUnit = null;
        for (BioTracUnit unit : bioTracUnitList) {
            if (unit.getUID().equals(uid)) {
                foundUnit = unit;
                break;
            }
        }
        if (foundUnit == null) {
            //Log.d(TAG, "Creating new unit: " + uid);
            if (reading == null) {
                foundUnit = new BioTracUnit(uid, "unknown");
            } else {
                foundUnit = new BioTracUnit(uid, reading.getName());
            }
            bioTracUnitList.add(foundUnit);
        }
        return foundUnit;
    }

    Boolean endStaleCheck = false;

    /**
     * checks for stale (no recent data) icons
     * makes them gray
     */
    public void run() {
        Log.d(TAG, "Stale thread starting");
//        Looper.prepare ();
        while (!endStaleCheck) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Log.e(TAG, "Interrupted exception: " + e.getMessage());
                return;
            }
            //CoordinatedTime currentTime = new CoordinatedTime();
            //Log.d (TAG, "Checking for stale - list size: " + bioTracUnitList.size());
            Iterator<BioTracUnit> iterator = bioTracUnitList.iterator();
            //for (BioTracUnit unit : bioTracUnitList) {
            while (iterator.hasNext()) {
                //Log.d (TAG, "Last reading: " + unit.getLastReading().getTime().toString() +
                //        "  Current time: " +
                //        currentTime.toString());
                BioTracUnit unit = iterator.next();
                if (unit.isDeleted()) {
                    Log.d(TAG, "Run: Removing " + unit.getUID());
                    iterator.remove();
                } else {
                    if (unit.getLastReading() != null) {
                        // test if this unit has gotten stale
                        if (CoordinatedTime.currentTimeMillis()
                                - unit.getLastReading().getTime().getMilliseconds() >
                                staleTime.getMilliseconds()) {
                            // update the icon color to gray
                            if (unit.getOverallAlertLevel() != BioTracAlertLevelEnum.GRAY) {
                                Log.d(TAG, "Changing alert " + unit.getOverallAlertLevel().toString() + " to gray");
                                unit.setOverallAlertLevel(BioTracAlertLevelEnum.GRAY);
                                displayEvent(unit.getLastReading(), BioTracAlertLevelEnum.GRAY);
                            } else {
                                // Log.d (TAG, "Alert level not changed");
                            }
                        }
                    }
                }
            }
//            Looper.loop ();
        }
        Log.i("TAG", "Check for stale exiting");
    }

    /**
     * examines all of the biometrics to determine the overall alert level
     * @param reading - the reading
     * @return - the overall alert level
     */
    private BioTracAlertLevelEnum determineOverallAlertLevel(BioTracReading reading) {
        BioTracAlertLevelEnum overallLevel = GREEN;
        BioTracMetric item;
        item = reading.getExertion();
        if (item != null) {
            overallLevel = compareLevels(overallLevel, item.getAlertLevel());
        }
        item = reading.getHeartRate();
        if (item != null) {
            overallLevel = compareLevels(overallLevel, item.getAlertLevel());
        }
        item = reading.getCoreTemp();
        if (item != null) {
            overallLevel = compareLevels(overallLevel, item.getAlertLevel());
        }
        item = reading.getHRLimitValue();
        if (item != null) {
            overallLevel = compareLevels(overallLevel, item.getAlertLevel());
        }
        item = reading.getStateOfCharge();
        if (item != null) {
            overallLevel = compareLevels(overallLevel, item.getAlertLevel());
        }
        item = reading.getFall();
        if (item != null) {
            overallLevel = compareLevels(overallLevel, item.getAlertLevel());
        }
        item = reading.getTap();
        if (item != null) {
            overallLevel = compareLevels(overallLevel, item.getAlertLevel());
        }
        item = reading.getNoMovement();
        if (item != null) {
            overallLevel = compareLevels(overallLevel, item.getAlertLevel());
        }
        item = reading.getHeartRateMinusAge();
        if (item != null) {
            overallLevel = compareLevels(overallLevel, item.getAlertLevel());
        }
        return overallLevel;
    }

    /**
     * compares two alert levels determining which is higher
     * @param overallLevel - the current level to compare
     * @param level - the level to compare it to
     * @return - new overall level
     */
    private BioTracAlertLevelEnum compareLevels(BioTracAlertLevelEnum overallLevel,
                                                BioTracAlertLevelEnum level) {
        BioTracAlertLevelEnum newLevel = GREEN;
        //Log.d (TAG, "compareLevels " + overallLevel + " " + level);
        switch (level) {
            case GREEN:
                // keep the old level
                newLevel = overallLevel;
                break;
            case YELLOW:
                if (overallLevel == YELLOW) {
                    // leave it unchanged
                    newLevel = overallLevel;
                } else {
                    if (overallLevel == GREEN) {
                        // accept the new level
                        newLevel = level;
                    } else {
                        // leave it unchanged
                        newLevel = overallLevel;
                    }
                }
                break;
            case RED:
                // accept the new level
                newLevel = level;
                break;
            case GRAY:
                Log.e(TAG, "compareLevels called with GRAY");
                break;
        }
        //Log.d(TAG, "compareLevels result: " + newLevel.toString());
        return newLevel;
    }

    private Boolean greenIsVisible = true;
    private Boolean yellowIsVisible = true;
    private Boolean redIsVisible = true;
    private Boolean grayIsVisible = true;
    /**
     * callback for when a checkbox it checked/unchecked
     * @param view - the view
     */
    @SuppressLint("NonConstantResourceId")
    public void checkboxCallback(View view) {
        Log.d(TAG, "CheckboxCallback " + view.getId() + " isChecked = " + ((CheckBox) view).isChecked());
        CheckBox cb = (CheckBox)view;
        switch (view.getId()) {
            case R.id.checkbox_showok:
                if (cb.isChecked ()) {
                    showHideIcons (GREEN, true);
                    greenIsVisible = true;
                } else {
                    showHideIcons (GREEN, false);
                    greenIsVisible = false;
                }
                break;
            case R.id.checkbox_showwarning:
                if (cb.isChecked ()) {
                    showHideIcons (YELLOW, true);
                    yellowIsVisible = true;
                } else {
                    showHideIcons (YELLOW, false);
                    yellowIsVisible = false;
                }
                break;
            case R.id.checkbox_showalert:
                if (cb.isChecked ()) {
                    showHideIcons (RED, true);
                    redIsVisible = true;
                } else {
                    showHideIcons (RED, false);
                    redIsVisible = false;
                }
                break;
            case R.id.checkbox_showstale:
                if (cb.isChecked ()) {
                    showHideIcons (GRAY, true);
                    grayIsVisible = true;
                } else {
                    showHideIcons (GRAY, false);
                    grayIsVisible = false;
                }
                break;
            default:
                Log.e (TAG, "Unimplemented checkbox handler");
                break;
        }
    }

    /**
     * Shows or hides icons based on the level
     * @param level - the alert level being affected
     * @param show - show (true) or hide the icon
     */
    private void showHideIcons (BioTracAlertLevelEnum level, Boolean show) {
        MapView mapView = getMapView();
        MapGroup mapGroup = mapView.getRootGroup().findMapGroup(
                "Drawing Objects");
        if (mapGroup == null) {
            Log.e(TAG, "HideIcons Map group is null");
            return;
        }
        Log.d (TAG, "unitlist has " + bioTracUnitList.size() + " entries");
        for (BioTracUnit unit : bioTracUnitList) {
            if (unit.getOverallAlertLevel() == level) {
                MapItem item = mapGroup.deepFindUID(unit.getUID());
                if (item != null) {
                    item.setVisible(show);
                    Log.d (TAG, "Unit " + unit.getUID() + " visible " + show + " " + level.toString());
                } else {
                    Log.w (TAG, "Unit not found " + unit.getUID() + " visible " + show + " " + level.toString());
                }
            }
        }
    }

    /**
     *
     */
    private String getBiotracRadialMenu() {
        return PluginMenuParser.getMenu(pluginContext, "biotrac_radial_menu.xml");
    }

    /**
     * Listens for map events
     */
    static class BioTracOnMapEventListener implements MapEventDispatcher.OnMapEventListener {

        @Override
        public void onMapItemMapEvent(MapItem mapItem, MapEvent mapEvent) {
            //Log.d(TAG, "onMapItemMapEvent " + mapEvent.getType().toString() + " " + mapItem.getUID());
            if (mapItem instanceof Marker) {
                //Log.d(TAG, "item is a " + mapItem.getClass().toString());

                switch (mapEvent.getType()) {
                    case "item_added":
                    case "item_press":
                    case "item_release":
                        break;
                    case "item_click":
                        // set the metadata so it displays
                        mapItem.setTitle(mapItem.getMetaString("reading", "No metadata"));
                        break;
                    case "item_confirmed_click":
                        // clear displayed metadata
                        mapItem.setTitle("");
                        break;
                    case "item_removed":
                        // flag the item for removal
                        String itemUID = mapItem.getUID();
                        for (BioTracUnit unit : bioTracUnitList) {
                            String uid = unit.getUID();
                            //Log.d(TAG, "Checking for removal " + uid + " " + itemUID);
                            if (uid.equals(itemUID)) {
                                unit.setDeleted(true);
                                Log.d(TAG, "Flagging for removal " + uid + " " + itemUID);
                            }
                        }
                        break;
                    default:
                        Log.d(TAG, "BioTracOnMapEventListener other type " + mapEvent.getType());
                        break;
                }
            }
        }
    }

    private static class GCListener implements MapItem.OnGroupChangedListener {

        @Override
        public void onItemAdded(MapItem mapItem, MapGroup mapGroup) {
            Log.d(TAG, "Item " + mapItem.getUID() + " group: " + mapGroup.getFriendlyName() + " added");
        }

        /**
         * Completes removal of the BioTracUnit
         *
         * @param mapItem - the MapItem
         * @param mapGroup - the map group
         */
        @Override
        public void onItemRemoved(MapItem mapItem, MapGroup mapGroup) {

            /* commented out for now - can't figure out how to update a marker, have to delete and create
            String uid = mapItem.getUID();
            for (BioTracUnit unit: bioTracUnitList) {
                if (unit.getUID().equals(uid)) {
                    bioTracUnitList.remove(unit);
                    Log.d (TAG, "Item " + uid + " group: " + mapGroup.getFriendlyName()
                            + " deleted - unit list has " + bioTracUnitList.size() + " entries");
                    break;
                }
            } */
        }
    }
}



