/*
 * BioTracReading.java
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * A single reading from the BioTrac
 *
 */
package com.atakmap.android.plugintemplate;

import com.atakmap.coremap.log.Log;
import com.atakmap.coremap.maps.coords.GeoPoint;
import com.atakmap.coremap.maps.time.CoordinatedTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BioTracReading {
    /**
     * a single reading
     * @Author jmerritt
     */
    public static final String TAG = "BioTrac";

    private BioTracMetric exertion = null; // uses value (%), threshold, level
    private BioTracMetric heartRate = null; // uses value (BPM), threshold, level
    private BioTracMetric coreTemp = null; // uses value (degrees C), threshold, level
    private BioTracMetric cadence = null;  // uses value (steps per minute)
    private BioTracMetric hssi = null; // uses value (%)
    private BioTracMetric hrLimitValue = null;  // uses value
    private BioTracMetric caloricBurnRate = null; // uses value (calories per ?)
    private BioTracMetric stateOfCharge = null; // uses value (%), level
    private BioTracMetric fall = null;  // uses level
    private BioTracMetric tap = null; // uses level
    private BioTracMetric noMovement = null; // uses level
    private BioTracMetric heartRateMinusAge = null; // uses level

    // present only if GPS is enabled and reporting
    private GeoPoint point = null;

    // time of this reading "yyyy-MM-ddTHH:mm:ss.SSSZ"
    private CoordinatedTime time;

    // id of the user
    private String id = "unknown";
    // uid reported
    private String uid = "unknown";
    // name of the user
    private String name = "unknown";
    // serial number of the band
    private String serialNumber = "unknown";

    public BioTracReading () {

    }
    public void setExertion (String value, String threshold, String level) {
        exertion = new BioTracMetric ("exertion", value, threshold, level);
    }

    public BioTracMetric getExertion () {
        return exertion;
    }

    public void setHeartRate (String value, String threshold, String level) {
        heartRate = new BioTracMetric ("heartrate", value, threshold, level);
    }

    public BioTracMetric getHeartRate () {
        return heartRate;
    }

    public void setCoreTemp (String value, String threshold, String level) {
        coreTemp = new BioTracMetric ("coretemp", value, threshold, level);
    }

    public BioTracMetric getCoreTemp () {
        return coreTemp;
    }

    public void setCadence (String cad) {
        cadence = new BioTracMetric ("cadence", cad, null, null);
    }

    public BioTracMetric getCadence () {
        return cadence;
    }

    public void setHSSI (String hssi) {
        this.hssi = new BioTracMetric ("hssi", hssi, null, null);
    }

    public BioTracMetric getHSSI () {
        return hssi;
    }

    public void setHRLimitValue (String value, String threshold, String level) {
        hrLimitValue = new BioTracMetric("hrlimitvalue", value, threshold, level);
    }

    public BioTracMetric getHRLimitValue () {
        return hrLimitValue;
    }

    public void setCaloricBurnRate (String cbr) {
        caloricBurnRate = new BioTracMetric ("caloricburnrate", cbr, null, null);
    }

    public BioTracMetric getCaloricBurnRate () {
        return caloricBurnRate;
    }

    public void setStateOfCharge (String value, String level) {
        stateOfCharge = new BioTracMetric ("stateofcharge", value, null, level);
    }

    public BioTracMetric getStateOfCharge () {
        return stateOfCharge;
    }

    public void setFall (String level) {
        fall = new BioTracMetric ("fall", null, null, level);
    }

    public BioTracMetric getFall () {
        return fall;
    }

    public void setTap (String tap) {
        this.tap = new BioTracMetric ("tap", null, null, tap);
    }

    public BioTracMetric getTap () {
        return tap;
    }

    public void setNoMovement (String nm) {
        noMovement = new BioTracMetric ("nomovement", null, null, nm);
    }

    public BioTracMetric getNoMovement () {
        return noMovement;
    }

    public void setHeartRateMinusAge (String nm) {
        heartRateMinusAge = new BioTracMetric ("heartrateminusage", null, null, nm);
    }

    public BioTracMetric getHeartRateMinusAge () {
        return heartRateMinusAge;
    }

    public void setGeoPoint (GeoPoint point) {
        this.point = point;
    }

    public GeoPoint getGeoPoint () {
        return point;
    }

    public void setName (String name) {
        this.name = name;
    }
    public String getName () {
        return name;
    }
    public String getID () {
        return id;
    }
    public String getUID () {
        return uid;
    }
    public void setUID (String uid) {
        this.uid = uid;
        this.id = uid.replace("BIOTRAC-", "");
    }
    public void setTime (String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
            Log.e (TAG, "Time Parse Exception setting time to now " + time);
            date = new Date ();
        }
        this.time = new CoordinatedTime(date.getTime());
    }

    public CoordinatedTime getTime () {
        return time;
    }
/*
        public String makeString () {
            String readingString = "HR: " + heartRate + " CoreTemp: " + coreTemp +
                    " Exer: " + exertion  + " cadence: " + cadence + " HSSI: " + hssi +
                    " Point:Lat: " + point.getLatitude() + " Lon: " + point.getLongitude() +
                    " Alt: " + point.getAltitude() +
                    " Time: " + time.toString();
            return readingString;
        }

 */
    public void setSerialNumber (String serial) { serialNumber = serial; }
    public String getSerialNumber () { return serialNumber; }
}
