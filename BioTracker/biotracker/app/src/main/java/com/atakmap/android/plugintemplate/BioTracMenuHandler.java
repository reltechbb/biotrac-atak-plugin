/*
 * <put file name here>
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * <put description here>
 *
 */
package com.atakmap.android.plugintemplate;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.annotation.NonNull;

import com.atakmap.android.plugintemplate.plugin.R;
import com.atakmap.coremap.log.Log;

public class BioTracMenuHandler {
    private Boolean audibleEnabled = Boolean.FALSE;
    private View view;
    private CheckboxListener cbListener;
    SwitchListener swListener;
    public static final String TAG = "BioTrac";
    private BioTracHandler handler;

    public BioTracMenuHandler(@NonNull View biotracView, BioTracHandler handler) {
        view = biotracView;
        this.handler = handler;
        swListener = new SwitchListener();
        Switch sw = (Switch) biotracView.findViewById(R.id.mute_toggle);
        sw.setChecked(audibleEnabled);
        sw.setOnClickListener(swListener);
        cbListener = new CheckboxListener(biotracView);
    }

    /**
     * listener for switch events
     */
    private class SwitchListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Switch sw = (Switch)view;
            audibleEnabled = sw.isChecked();
        }
    }

    /**
     * listener for checkbox events events
     *
     */
    private class CheckboxListener implements View.OnClickListener {

        public CheckboxListener(View biotracView) {
            CheckBox cb = (CheckBox) biotracView.findViewById(R.id.checkbox_showok);
            cb.setOnClickListener(this);
            cb = (CheckBox) biotracView.findViewById(R.id.checkbox_showwarning);
            cb.setOnClickListener(this);
            cb = (CheckBox) biotracView.findViewById(R.id.checkbox_showalert);
            cb.setOnClickListener(this);
            cb = (CheckBox) biotracView.findViewById(R.id.checkbox_showstale);
            cb.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            handler.checkboxCallback(view);
        }
    }

    public Boolean isAudibleEnabled() {
        return audibleEnabled;
    }

    /**
     * dispose of anything you added upon removal of the plugin
     */
    public void dispose() {
        // nothing to do at this time
    }
}
