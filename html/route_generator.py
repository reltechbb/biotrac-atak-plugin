Route = '''

@app.route('/new_route')
def route_name():
    return render_template('template_route', pages=page_generator())
'''


def route_generator(app):
    f = open("app.py", "a")
    index = 0
    for template_path in app.jinja_env.list_templates():
        try:
            page = str(template_path)
            if '.html' in page:
                if 'jama/Matrix' in page:
                    name = 'route' + str(index)
                    index += 1
                    f.write(Route.replace("new_route",page).replace("route_name",name).replace("template_route","Matrix.html"))
                else:    
                    name = 'route' + str(index)
                    index += 1
                    f.write(Route.replace("new_route",page).replace("route_name",name).replace("template_route",page))
            else:
                name = 'route' + str(index)
                index += 1
                f.write(Route.replace("new_route","static/" + page).replace("route_name",name).replace("template_route",page))
        
        except Exception as e:
            print(str(e))
    f.close()
