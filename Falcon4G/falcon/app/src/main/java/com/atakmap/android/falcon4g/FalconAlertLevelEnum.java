/*
 *
 *  * <put file name here>
 *  *
 *  * Copyright (c) 2001-2022 Domenix
 *  * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *  *
 *  * <put description here>
 *  *
 *
 */

package com.atakmap.android.falcon4g;

public enum FalconAlertLevelEnum {
    // green - no alert
    // yellow - caution
    // red - alert
    // gray - stale
    GREEN, YELLOW, RED, GRAY
}
