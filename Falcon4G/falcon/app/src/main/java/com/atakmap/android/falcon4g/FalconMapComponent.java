
/*
 * <put file name here>
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * <put description here>
 *
 */
package com.atakmap.android.falcon4g;

import android.content.Context;
import android.content.Intent;
import com.atakmap.android.ipc.AtakBroadcast.DocumentedIntentFilter;

import com.atakmap.android.maps.MapView;
import com.atakmap.android.dropdown.DropDownMapComponent;

import com.atakmap.coremap.log.Log;
import com.atakmap.android.falcon4g.plugin.R;

public class FalconMapComponent extends DropDownMapComponent {

    private static final String TAG = "Falcon";

    private Context pluginContext;

    private FalconDropDownReceiver ddr;

    public void onCreate(final Context context, Intent intent,
            final MapView view) {

        context.setTheme(R.style.ATAKPluginTheme);
        super.onCreate(context, intent, view);
        pluginContext = context;

        ddr = new FalconDropDownReceiver(
                view, context);

        Log.d(TAG, "registering the plugin filter");
        DocumentedIntentFilter ddFilter = new DocumentedIntentFilter();
        ddFilter.addAction(FalconDropDownReceiver.SHOW_PLUGIN);
        registerDropDownReceiver(ddr, ddFilter);
    }

    @Override
    protected void onDestroyImpl(Context context, MapView view) {
        super.onDestroyImpl(context, view);
    }

}
