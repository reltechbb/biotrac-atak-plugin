/*
 * <put file name here>
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * <put description here>
 *
 */
package com.atakmap.android.falcon4g;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Switch;

import androidx.annotation.NonNull;

import com.atakmap.android.falcon4g.plugin.R;

public class FalconMenuHandler {
    private Boolean audibleEnabled = Boolean.FALSE;
    private View view;
    private CheckboxListener cbAudibleListener;
    private SwitchAudibleListener swAudibleListener;
    public static final String TAG = "Falcon";
    private FalconHandler handler;

    // delete one switch
    private SwitchDeleteOneListener deleteOneListener;
    private Boolean deleteOneEnabled = Boolean.TRUE;

    // delete all button
    //private Button deleteAllButton;
    private DeleteAllListener deleteAllListener;

    // delete stale switch
    private SwitchDeleteStaleListener deleteStaleListener;
    private Boolean deleteStaleEnabled = Boolean.TRUE;

    public FalconMenuHandler(@NonNull View secView, FalconHandler handler) {
        view = secView;
        this.handler = handler;
        swAudibleListener = new SwitchAudibleListener();
        Switch swAudible = (Switch) secView.findViewById(R.id.mute_toggle);
        swAudible.setChecked(audibleEnabled);
        swAudible.setOnClickListener(swAudibleListener);
        cbAudibleListener = new CheckboxListener(secView);

        deleteOneListener = new SwitchDeleteOneListener ();
        Switch swDeleteOne = (Switch) secView.findViewById(R.id.deleteToggle);
        swDeleteOne.setChecked(deleteOneEnabled);
        swDeleteOne.setOnClickListener(deleteOneListener);

        deleteStaleListener = new SwitchDeleteStaleListener ();
        Switch swDeleteStale = (Switch) secView.findViewById(R.id.staleToggle);
        swDeleteStale.setChecked(deleteStaleEnabled);
        swDeleteStale.setOnClickListener(deleteStaleListener);

        deleteAllListener = new DeleteAllListener (view);
        Button button = view.findViewById(R.id.deleteAllButton);
        button.setOnClickListener(deleteAllListener);
    }

    /**
     * listener for switch events
     */
    private class SwitchAudibleListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Switch sw = (Switch)view;
            audibleEnabled = sw.isChecked();
        }
    }
    /**
     * listener for switch events
     */
    private class SwitchDeleteOneListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Switch sw = (Switch)view;
            deleteOneEnabled = sw.isChecked();
            Log.d (TAG, "Delete one enabled: " + deleteOneEnabled);
        }
    }
    /**
     * listener for switch events
     */
    private class SwitchDeleteStaleListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Switch sw = (Switch)view;
            deleteStaleEnabled = sw.isChecked();
            Log.d (TAG, "Delete stale enabled: " + deleteStaleEnabled);
        }
    }
    /**
     * listener for checkbox events events
     *
     */
    private class CheckboxListener implements View.OnClickListener {

        public CheckboxListener(View view) {
            CheckBox cb = (CheckBox) view.findViewById(R.id.checkbox_showok);
            cb.setOnClickListener(this);
            cb = (CheckBox) view.findViewById(R.id.checkbox_showwarning);
            cb.setOnClickListener(this);
            cb = (CheckBox) view.findViewById(R.id.checkbox_showalert);
            cb.setOnClickListener(this);
            cb = (CheckBox) view.findViewById(R.id.checkbox_showstale);
            cb.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            handler.checkboxCallback(view);
        }
    }

    private class DeleteAllListener implements View.OnClickListener {
        public DeleteAllListener (View view) {
            Button button = (Button) view.findViewById(R.id.deleteAllButton);
            button.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            handler.deleteAllCallback(view);
        }
    }
    public Boolean isAudibleEnabled() {
        return audibleEnabled;
    }
    public Boolean isDeleteOneEnabled () { return deleteOneEnabled; }
    public Boolean isKeepStaleEnabled () { return deleteStaleEnabled; }

    /**
     * dispose of anything you added upon removal of the plugin
     */
    public void dispose() {
        // nothing to do at this time
    }
}
