/*
 * <put file name here>
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * <put description here>
 *
 */
package com.atakmap.android.falcon4g;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.atakmap.android.maps.MapView;
import com.atakmap.comms.CommsMapComponent;
import com.atakmap.comms.CotServiceRemote;
import com.atakmap.comms.CotStreamListener;
import com.atakmap.comms.TAKServerListener;
import com.atakmap.comms.app.CotPortListActivity;
import com.atakmap.coremap.cot.event.CotDetail;
import com.atakmap.coremap.cot.event.CotEvent;
import com.atakmap.coremap.log.Log;
import com.atakmap.coremap.maps.coords.GeoPoint;

import java.util.List;

import static com.atakmap.android.maps.MapView.getMapView;

public class FalconCOTReceiver {
    public static final String TAG = "Falcon";

    private CotServiceRemote.CotEventListener eventListener;
    private FalconHandler handler;
    private Context pluginContext;

    public FalconCOTReceiver(Context context, View pluginView) {
        pluginContext = context;

        handler = new FalconHandler (pluginContext, pluginView);

        // This code creates a listener for COT messages
        // If the type starts with "y-" the icon is not plotted
        CotServiceRemote.CotEventListener eventListener = new CotServiceRemote.CotEventListener() {
             @Override
            public void onCotEvent(CotEvent cotEvent, Bundle bundle) {
/*                 Log.i (TAG, "COT Event Received - type: " + cotEvent.getType());
                 Log.i (TAG, "COT Event Received - UID: " + cotEvent.getUID());
                 Log.i (TAG, "COT Event Received - Start: " + cotEvent.getStart().toString());
                 GeoPoint point = cotEvent.getGeoPoint();
                 Log.i (TAG, "COT Event Received - latitude: " + point.getLatitude());
                 CotDetail detail = cotEvent.getDetail();
                 List<CotDetail> detailList = detail.getChildren();
                 for (CotDetail detailItem: detailList) {
                     Log.i(TAG, "Element name: " + detailItem.getElementName()
                             + " Value: " + detailItem.getInnerText());
                 }

 */
                 handler.processEvent (cotEvent);
            }
        };
        // this registers the listener to receive the COT messages
        CommsMapComponent.getInstance().addOnCotEventListener(eventListener);

        Log.i (TAG, "Falcon COT Receiver constructed");
    }

    /**
     * Called to free all resources for example when a new version is loaded
     * You should remove anything that was "add"ed
     */
    public void dispose () {
        handler.dispose();
        CommsMapComponent.getInstance().removeOnCotEventListener(eventListener);
    }

}
