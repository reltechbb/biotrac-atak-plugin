/**
 *
 *  FalconReadingGroup.java
 *
 *  Copyright (c) 2001-2022 Domenix
 *
 *  A group of Readings
 *
 */

package com.atakmap.android.falcon4g;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FalconReadingGroup {
    private String groupID;
    private Boolean groupDeleted = false;

    private ArrayList<FalconReading> readingList = new ArrayList<FalconReading>();

    public FalconReadingGroup (String value) {
        groupID = value;
    }

    public void addReading (FalconReading item) {
        readingList.add(item);
    }
    public ArrayList<FalconReading> getReadingList () {
        return readingList;
    }
    public String getGroupID () { return groupID; }

    public void setGroupDeleted (Boolean value) {
        groupDeleted = value;
    }
    public Boolean getGroupDeleted () {
        return groupDeleted;
    }
}
