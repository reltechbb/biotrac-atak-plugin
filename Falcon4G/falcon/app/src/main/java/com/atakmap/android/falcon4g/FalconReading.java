/**
 * FalconReading.java
 *
 * Copyright (c) 2001-2022 Domenix
 *
 * A single reading from the Falcon4G
 *
 */
package com.atakmap.android.falcon4g;

import com.atakmap.coremap.log.Log;
import com.atakmap.coremap.maps.coords.GeoPoint;
import com.atakmap.coremap.maps.time.CoordinatedTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FalconReading {
    /**
     * a single reading
     * @author jmerritt
     */
    public static final String TAG = "Falcon";

    // time of this reading "yyyy-MM-ddTHH:mm:ss.SSSZ"
    private CoordinatedTime time;
    private long timeMillisec;
    // time message was received
    private long receivedTime = new Date().getTime();

    private String detectionType = "UNKNOWN";
    private Double concentration;
    private String materialName = "UNKNOWN";
    private String materialClass = "UNKNOWN";
    private String serviceNumber = "UNKNOWN";
    private String persistency = "UNKNOWN";
    private Double confidence = 0.0;
    private String uid;
//    private Double latitude = null;
//    private Double longitude = null;
    private String groupID;
    private Boolean harmful = false;
    private Boolean deleted = false;
    private GeoPoint geoPoint = new GeoPoint (0.0, 0.0);

    private CoordinatedTime staleTime;
    private long staleTimeMillisec;
    private long staleDelay;

    public FalconReading () {

    }

    public void setDetectionType (String value) {
        detectionType = value;
    }

    public String getDetectionType () {
        return detectionType;
    }

    public void setConcentration (String value) {
        concentration = Double.parseDouble(value);
    }

    public Double getConcentration () {
        return concentration;
    }

    public void setMaterialName (String value) {
        materialName = value;
    }

    public String getMaterialName () {
        return materialName;
    }

    public void setMaterialClass (String value) {
        materialClass = value;
    }

    public String getMaterialClass () {
        return materialClass;
    }

    public void setServiceNumber (String value) {
        serviceNumber = value;
    }

    public String getServiceNumber () {
        return serviceNumber;
    }

    public void setPersistency (String value) {
        persistency = value;
    }

    public String getPersistency () {
        return persistency;
    }

    public void setConfidence (String value) {
        confidence = Double.parseDouble(value);
    }

    public Double getConfidence () { return confidence; }

    public void setTime (String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
            Log.e (TAG, "Time Parse Exception setting time to now " + time);
            date = new Date ();
        }
        timeMillisec = date.getTime();
        this.time = new CoordinatedTime(timeMillisec);
    }

    public CoordinatedTime getTime () {
        return time;
    }
    public long getTimeMillisec () { return timeMillisec; }

    public void setUID (String value) {
        uid = value;
    }

    public String getUID () {
        return uid;
    }

    public void setGroupID (String value) {
        groupID = value;
    }
    public String getGroupID () {
        return groupID;
    }

    public void setHarmful (String value) {
        harmful = Boolean.valueOf(value);
    }
    public Boolean getHarmful() {
        return harmful;
    }
    public FalconAlertLevelEnum getAlertLevel () {
        FalconAlertLevelEnum level = FalconAlertLevelEnum.GREEN;
        if (harmful) {
            level = FalconAlertLevelEnum.RED;
        }
        return level;
    }

    public void setDeleted (Boolean value) {
        deleted = value;
    }
    public Boolean getDeleted () {
        return deleted;
    }

    public void setGeoPoint (GeoPoint value) {
        geoPoint = value;
    }
    public GeoPoint getGeoPoint () {
        return geoPoint;
    }

    public void setStaleTime (CoordinatedTime value) {
        staleTime = value;
        staleTimeMillisec = value.getMilliseconds();
    }
    public CoordinatedTime getStaleTime () {
        return staleTime;
    }

    public void setStaleDelay (long value) {
        staleDelay = value;
    }
   public long getStaleDelayMillisec () {
        return staleDelay;
    }
    public long getReceivedTimeMillisec() {
        return receivedTime;
    }
}
