/*
 * <put file name here>
 *
 * Copyright (c) 2001-2022 Domenix
 * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *
 * <put description here>
 *
 */
package com.atakmap.android.falcon4g;

import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.view.View;

import com.atak.plugins.impl.PluginLayoutInflater;
import com.atakmap.android.maps.MapView;
import com.atakmap.android.falcon4g.plugin.R;
import com.atakmap.android.dropdown.DropDown.OnStateListener;
import com.atakmap.android.dropdown.DropDownReceiver;

import com.atakmap.android.toolbar.widgets.TextContainer;
import com.atakmap.coremap.log.Log;

public class FalconDropDownReceiver extends DropDownReceiver implements
        OnStateListener {

    public static final String TAG = "Falcon";

    public static final String SHOW_PLUGIN = "com.atakmap.android.falcon4g.SHOW_PLUGIN";
    private final View pluginView;
    private final Context pluginContext;

    private final com.atakmap.android.falcon4g.FalconCOTReceiver receiver;
    /**************************** CONSTRUCTOR *****************************/

    public FalconDropDownReceiver(final MapView mapView,
                                   final Context context) {
        super(mapView);
        this.pluginContext = context;

        // Remember to use the PluginLayoutInflator if you are actually inflating a custom view
        // In this case, using it is not necessary - but I am putting it here to remind
        // developers to look at this Inflator
        pluginView = PluginLayoutInflater.inflate(context,
                R.layout.main_layout, null);

        receiver = new FalconCOTReceiver(pluginContext, pluginView);
    }

    /**************************** PUBLIC METHODS *****************************/
    @Override
    public void disposeImpl() {
        Log.d (TAG, " Dispose called");
        receiver.dispose();
    }

    /**************************** INHERITED METHODS *****************************/

    @Override
    public void onReceive(Context context, Intent intent) {

        final String action = intent.getAction();
        if (action == null)
            return;

        if (action.equals(SHOW_PLUGIN)) {

            Log.d(TAG, "showing plugin drop down");
            showDropDown(pluginView, HALF_WIDTH, FULL_HEIGHT, FULL_WIDTH,
                    HALF_HEIGHT, false, this);
        }
    }

    @Override
    public void onDropDownSelectionRemoved() {
    }

    @Override
    public void onDropDownVisible(boolean v) {
    }

    @Override
    public void onDropDownSizeChanged(double width, double height) {
    }

    @Override
    public void onDropDownClose() {
    }

}
