/*
 *
 *  * <put file name here>
 *  *
 *  * Copyright (c) 2001-2022 Domenix
 *  * Unlimited use rights by Joint Program Executive Office Biological Chemical Command (DOD)
 *  *
 *  * <put description here>
 *  *
 *
 */

package com.atakmap.android.falcon4g;

import static com.atakmap.android.maps.MapView.getMapView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class FalconAlertDialog implements View.OnClickListener{
    public static final String TAG = "Falcon";
    private static AlertDialog alertDialog;

    public void alertDialog(Context pluginContext, String text) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(pluginContext);
        Log.i(TAG, "alert message: " + text);
        TextView showText = new TextView(getMapView().getContext());
        showText.setText(text);
        showText.setTextIsSelectable(true);

        builderSingle.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                alertDialog = null;
            }
        });
        builderSingle.setCancelable(false);
        builderSingle.setTitle("Falcon Alert");
        builderSingle.setView(showText);
        alertDialog = builderSingle.create();
        alertDialog.show();
    }

    @Override
    public void onClick(View view) {

    }
}
