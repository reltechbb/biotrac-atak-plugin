/**
 * FalconHandler.java
 *
 * Copyright (c) 2001-2022 Domenix
 *
 * <put description here>
 *
 */
package com.atakmap.android.falcon4g;

import static com.atakmap.android.maps.MapView.getMapView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.atakmap.android.maps.MapEvent;
import com.atakmap.android.maps.MapEventDispatcher;
import com.atakmap.android.maps.MapGroup;
import com.atakmap.android.maps.MapItem;
import com.atakmap.android.maps.MapView;
import com.atakmap.android.maps.Marker;
import com.atakmap.android.menu.PluginMenuParser;
import com.atakmap.android.falcon4g.plugin.R;
import com.atakmap.coremap.cot.event.CotDetail;
import com.atakmap.coremap.cot.event.CotEvent;
import com.atakmap.coremap.maps.assets.Icon;
import com.atakmap.coremap.maps.time.CoordinatedTime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FalconHandler  implements Runnable {
    private static Thread thread = null;
    public static final String TAG = "Falcon";
    private static final String FALCON_TYPE = "y-f-G-U-D";
    private static final ArrayList<FalconReadingGroup> groupList = new ArrayList<>();
    private final Context pluginContext;
    private static FalconMenuHandler menuHandler;

    /**
     * constructor
     * @param context - the context
     * @param pluginView - the view
     */
    public FalconHandler(Context context, View pluginView) {
        MapView mapView = getMapView();
        if (mapView.getRootGroup().findMapGroup(
                TAG) == null) {
            mapView.getRootGroup().addGroup("TAG");
            Log.d(TAG, "Creating Map Group: " + TAG);
            // todo figure out why we can't find this group
        } else {
            Log.d(TAG, "Map Group: " + TAG + " already exists");
        }
        pluginContext = context;
        menuHandler = new FalconMenuHandler(pluginView, this);
        thread = new Thread(this);
        thread.start();

        Log.i(TAG, "Falcon handler started");
    }

    /**
     * process a COT event
     * @param event - the COT event
     */
    public void processEvent(CotEvent event) {
        Log.d(TAG, "ProcessEvent:Type: " + event.getType());
        FalconReading newReading = null;
        // discard anything received with wrong type
        if (event.getType().equals(FALCON_TYPE)) {
            String uid = event.getUID();

            CotDetail detail = event.getDetail();
            List<CotDetail> detailList = detail.getChildren();
            CoordinatedTime staleTime = event.getStale();
            CoordinatedTime startTime = event.getStart();
            long staleDelay = staleTime.getMilliseconds() - startTime.getMilliseconds();
            //Log.d (TAG, "DetailList has " + detailList.size() + " children");
            // detailList has zero or more readings
            for (CotDetail reading : detailList) {
                List<CotDetail> readingList = reading.getChildren();
                // readingList has a child for each time, name metric, etc.
                //Log.d (TAG, "ReadingList has " + readingList.size() + " children");
                newReading = new FalconReading();
                newReading.setStaleTime(staleTime);
                newReading.setStaleDelay(staleDelay);
                newReading.setGeoPoint(event.getGeoPoint());
                for (CotDetail readingItem : readingList) {
                    newReading.setUID(uid);
                    switch (readingItem.getElementName()) {
                        // time of the event
                        case "time":
                            newReading.setTime(readingItem.getInnerText());
                            break;
                        case "detectionType":
                            newReading.setDetectionType(readingItem.getAttribute("value"));
                            break;
                        case "concentration":
                            newReading.setConcentration(readingItem.getAttribute("value"));
                            break;
                        case "materialName":
                            newReading.setMaterialName(readingItem.getAttribute("value"));
                            break;
                        case "materialClass":
                            newReading.setMaterialClass(readingItem.getAttribute("value"));
                            break;
                        case "serviceNumber":
                            newReading.setServiceNumber(readingItem.getAttribute("value"));
                            break;
                        case "persistency":
                            newReading.setPersistency(readingItem.getAttribute("value"));
                            break;
                        case "confidence":
                            newReading.setConfidence(readingItem.getAttribute("value"));
                            break;
//                        case "latitude":
//                            newReading.setLatitude(readingItem.getAttribute("value"));
//                            break;
//                        case "longitude":
//                            newReading.setLongitude(readingItem.getAttribute("value"));
//                            break;
                        case "groupID":
                            newReading.setGroupID(readingItem.getAttribute("value"));
                            break;
                        case "harmful":
                            newReading.setHarmful(readingItem.getAttribute("value"));
                            break;
                        default:
                            Log.e(TAG, "Unexpected element name: " + readingItem.getElementName());
                            break;
                    }
                }
            }
            // get the reading group, creating it if necessary
            FalconReadingGroup group = getGroup(newReading);
            group.addReading(newReading);

            // display the icon if we have location data
            if (newReading != null) {
                displayEvent(newReading);
            } else {
                // todo do something here
            }
        }
    }

    /**
     * Called when the plugin is removed or replaced to
     * free anything you "add"ed
     */
    public void dispose() {
        endStaleCheck = true;
        synchronized (thread) {
            thread.notify();
        }
        MapView mapView = getMapView();
        MapGroup mapGroup = mapView.getRootGroup().findMapGroup(
                "Drawing Objects");
        if (mapGroup == null) {
            Log.e(TAG, "dispose: Map group is null");
            return;
        }
        // remove any existing map icons
        Log.d(TAG, "removing existing icons");
        for (FalconReadingGroup group : groupList) {
            for (FalconReading reading : group.getReadingList()) {
                String uid = reading.getUID();
                // find existing map item
                MapItem item = mapGroup.deepFindUID(uid);
                // if item exists, remove it
                if (item != null) {
                    Log.d(TAG, "Dispose removing " + uid);
                    mapGroup.removeItem(item);
                } else {
                    Log.d(TAG, "couldn't find " + uid);
                }
            }
        }
        menuHandler.dispose();
    }

    private FalconAlertDialog dialog = new FalconAlertDialog();

    /**
     * displays the event on the map
     *
     * @param reading - the processed reading
     */
    private void displayEvent(FalconReading reading) {
        boolean newMarker;
        MapView mapView = getMapView();
        MapGroup mapGroup = mapView.getRootGroup().findMapGroup(
//                TAG);
                "Drawing Objects");
        if (mapGroup == null) {
            Log.e(TAG, "Map group is null");
            return;
        }
        // find existing map item
        MapItem item = mapGroup.deepFindUID(reading.getUID());
        // controls which icons are on top - lowest first
        // this has no effect
        //Double zOrder = 10.0;
        Marker marker;
        if (item == null) {
            // create a new marker since one does not exist
            Log.d (TAG, "Creating marker uid: " + reading.getUID());
            marker = new Marker(reading.getGeoPoint(), reading.getUID());
            newMarker = true;
        } else {
            // update the existing marker
            marker = (Marker) item;
            newMarker = false;
        }
        // build the new map item
        // !! GeoPoint cannot be null
        Icon.Builder iBuilder = null;
        Boolean markerVisible = true;
        switch (reading.getAlertLevel()) {
            case GREEN:
                iBuilder = new Icon.Builder().setImageUri(0,
                        "android.resource://" + pluginContext.getPackageName()
                        + "/" + R.drawable.nochem);
                markerVisible = greenIsVisible;
                //zOrder = 20.0;
                break;
            case YELLOW:
                iBuilder = new Icon.Builder().setImageUri(0,
                        "android.resource://" + pluginContext.getPackageName()
                                + "/" + R.drawable.ss_icon_yellow);
                markerVisible = yellowIsVisible;
                //zOrder = 30.0;
                break;
            case RED:
                iBuilder = new Icon.Builder().setImageUri(0,
                        "android.resource://" + pluginContext.getPackageName()
                        + "/" + R.drawable.chemred);
                alertTone();
                markerVisible = redIsVisible;
                break;
            case GRAY:
                iBuilder = new Icon.Builder().setImageUri(0,
                        "android.resource://" + pluginContext.getPackageName()
                                + "/" + R.drawable.ss_icon_gray);
                markerVisible = grayIsVisible;
                //zOrder = 10.0;
                //alertDialog ("Some text");
                break;
        }
        marker.setPoint(reading.getGeoPoint());
        marker.setIcon(iBuilder.build());
        // set if this marker should be visible
        marker.setVisible(markerVisible);
        marker.setMetaString("reading", buildMetadata(reading));

        //marker.setZOrder(zOrder);
        // this is a new marker so have to add additional things
        if (newMarker) {
            // make the marker removable
            marker.setMetaBoolean("removable", true);
            // make the marker clickable
            marker.setClickable(true);
            //marker.setAlwaysShowText(true);
            // set a custom radial menu
            marker.setMetaString("menu", getFalconRadialMenu());

            MapEventDispatcher dispatcher = mapView.getMapEventDispatcher();
            dispatcher.addMapItemEventListener(marker,
                    new FalconOnMapEventListener());
            // lets you know when items deleted
            marker.addOnGroupChangedListener(new GCListener());
            mapGroup.addItem(marker);
        }
    }

    /**
     * generates a tone if not muted
     */
    private void alertTone() {
        if (menuHandler.isAudibleEnabled()) {
            //Log.d(TAG, "Audible Alert Enabled2: " + menuHandler.isAudibleEnabled());
            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
            toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
        }
    }

    /**
     * builds the metadata to display when icon is tapped
     * @param reading - the reading to build the metadata string from
     * @return - the metadata string
     */
    private String buildMetadata(FalconReading reading) {
        StringBuilder builder = new StringBuilder();
        // Note: html doesn't work in these strings
        builder.append("Event Time: ").append(reading.getTime().toString()).append("\n");
        builder.append("Detection Type: ").append(reading.getDetectionType()).append("\n");
        builder.append("Concentration: ").append(reading.getConcentration()).append(" Kgpm3\n");
        builder.append("Material Name: ").append(reading.getMaterialName()).append("\n");
        builder.append("Material Class: ").append(reading.getMaterialClass()).append("\n");
        builder.append("Service Number: ").append(reading.getServiceNumber()).append("\n");
        builder.append("Persistency: ").append(reading.getPersistency()).append("\n");

        return builder.toString();
    }

    /**
     * finds an existing reding group
     * creates one if necessary
     * @param reading - current reading
     * @return - the existing or new unit
     */
    private FalconReadingGroup getGroup(FalconReading reading) {
        FalconReadingGroup foundGroup = null;
        for (FalconReadingGroup group : groupList) {
            if (group.getGroupID().equals(reading.getGroupID())) {
                foundGroup = group;
                break;
            }
        }
        if (foundGroup == null) {
            Log.d(TAG, "Creating new reading group: " + reading.getGroupID());
            if (reading != null) {
                foundGroup = new FalconReadingGroup (reading.getGroupID());
                groupList.add(foundGroup);
            }
        }
        return foundGroup;
    }

    Boolean endStaleCheck = false;

    /**
     * checks for items that were deleted
     */
    public void run() {
        Log.d(TAG, "Stale thread starting");
        MapView mapView = getMapView();
        MapGroup mapGroup = mapView.getRootGroup().findMapGroup(
                "Drawing Objects");
//        Looper.prepare ();
        while (!endStaleCheck) {
            synchronized (thread) {
                try {
                    thread.wait (10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Iterator<FalconReadingGroup> groupIterator = groupList.iterator();
            while (groupIterator.hasNext()) {
                FalconReadingGroup group = groupIterator.next();
                if (group.getGroupDeleted()) {
                    // remove all of the icons for this group
                    for (FalconReading reading : group.getReadingList()) {
                        String uid = reading.getUID();
                        // find existing map item
                        MapItem item = mapGroup.deepFindUID(uid);
                        // if item exists, remove it
                        if (item != null) {
                            Log.d(TAG, "Removing group " + uid);
                            mapGroup.removeItem(item);
                        } else {
                            Log.d(TAG, "remove group - couldn't find " + uid);
                        }
                    }
                    // remove this group from the list
                    groupIterator.remove();

                } else {
                    // check individual readings for deletion
                    Iterator<FalconReading> readingIterator = group.getReadingList().iterator();
                    long now = System.currentTimeMillis();
                    while (readingIterator.hasNext()) {
                        // if the item is flagged for deletion or the stale time has passed
                        // delete the item
                        FalconReading reading = readingIterator.next();
                        //Log.d (TAG, "Now: " + now + " stale: " + reading.getReceivedTimeMillisec()
                        //+ " delay: " + reading.getStaleDelayMillisec());
                        if (reading.getDeleted() || ((now > reading.getReceivedTimeMillisec()
                                + reading.getStaleDelayMillisec())
                                && !menuHandler.isKeepStaleEnabled())) {
                            String uid = reading.getUID();
                            // find existing map item
                            MapItem item = mapGroup.deepFindUID(uid);
                            // if item exists, remove it
                            if (item != null) {
                                // remove from data structure
                                Log.d(TAG, "Stale thread removing " + uid);
                                mapGroup.removeItem(item);
                            } else {
                                Log.d(TAG, "Stale thread remove 1 - couldn't find " + uid);
                            }
                            // remove from map
                            readingIterator.remove();
                        }
                    }
                }
            }
//            Looper.loop ();
        }
        Log.i("TAG", "Check for stale exiting");
    }

    private Boolean greenIsVisible = true;
    private Boolean yellowIsVisible = true;
    private Boolean redIsVisible = true;
    private Boolean grayIsVisible = true;

    /**
     * callback for when a checkbox it checked/unchecked
     * @param view - the view
     */
    @SuppressLint("NonConstantResourceId")
    public void checkboxCallback(View view) {
        //Log.d(TAG, "CheckboxCallback " + view.getId() + " isChecked = " + ((CheckBox) view).isChecked());
        CheckBox cb = (CheckBox)view;
        switch (view.getId()) {
            case R.id.checkbox_showok:
                if (cb.isChecked ()) {
                    showHideIcons (FalconAlertLevelEnum.GREEN, true);
                    greenIsVisible = true;
                } else {
                    showHideIcons (FalconAlertLevelEnum.GREEN, false);
                    greenIsVisible = false;
                }
                break;
            case R.id.checkbox_showwarning:
                if (cb.isChecked ()) {
                    showHideIcons (FalconAlertLevelEnum.YELLOW, true);
                    yellowIsVisible = true;
                } else {
                    showHideIcons (FalconAlertLevelEnum.YELLOW, false);
                    yellowIsVisible = false;
                }
                break;
            case R.id.checkbox_showalert:
                if (cb.isChecked ()) {
                    showHideIcons (FalconAlertLevelEnum.RED, true);
                    redIsVisible = true;
                } else {
                    showHideIcons (FalconAlertLevelEnum.RED, false);
                    redIsVisible = false;
                }
                break;
            case R.id.checkbox_showstale:
                if (cb.isChecked ()) {
                    showHideIcons (FalconAlertLevelEnum.GRAY, true);
                    grayIsVisible = true;
                } else {
                    showHideIcons (FalconAlertLevelEnum.GRAY, false);
                    grayIsVisible = false;
                }
                break;
            default:
                Log.e (TAG, "Unimplemented checkbox handler");
                break;
        }
    }

    /**
     * Shows or hides icons based on the level
     * @param level - the alert level being affected
     * @param show - show (true) or hide the icon
     */
    private void showHideIcons (FalconAlertLevelEnum level, Boolean show) {
        MapView mapView = getMapView();
        MapGroup mapGroup = mapView.getRootGroup().findMapGroup(
                "Drawing Objects");
        if (mapGroup == null) {
            Log.e(TAG, "HideIcons Map group is null");
            return;
        }
        for (FalconReadingGroup group : groupList) {
            for (FalconReading reading : group.getReadingList())
            if (reading.getAlertLevel() == level) {
                MapItem item = mapGroup.deepFindUID(reading.getUID());
                if (item != null) {
                    item.setVisible(show);
                    Log.d(TAG, "Unit " + reading.getUID() + " visible " + show + " " + level.toString());
                } else {
                    Log.w(TAG, "Unit not found " + reading.getUID() + " visible " + show + " " + level.toString());
                }
            }
        }
    }


    /**
     *
     */
    private String getFalconRadialMenu() {
        return PluginMenuParser.getMenu(pluginContext, "falcon_radial_menu.xml");
    }

    /**
     * Listens for map events
     */
    static class FalconOnMapEventListener implements MapEventDispatcher.OnMapEventListener {

        @Override
        public void onMapItemMapEvent(MapItem mapItem, MapEvent mapEvent) {
            if (mapItem instanceof Marker) {
                switch (mapEvent.getType()) {
                    case "item_added":
                    case "item_press":
                    case "item_release":
                        break;
                    case "item_click":
                        // set the metadata so it displays
                        mapItem.setTitle(mapItem.getMetaString("reading", "No metadata"));
                        break;
                    case "item_confirmed_click":
                        // clear displayed metadata
                        mapItem.setTitle("");
                        break;
                    case "item_removed":
                        // flag the item for removal
                        String itemUID = mapItem.getUID();
                        for (FalconReadingGroup group : groupList) {
                            for (FalconReading reading : group.getReadingList()) {
                                String uid = reading.getUID();
                                //Log.d(TAG, "Checking for removal " + uid + " " + itemUID);
                                if (uid.equals(itemUID)) {
                                    if (menuHandler.isDeleteOneEnabled()) {
                                        // flag just the one for deletion
                                        reading.setDeleted(true);
                                        Log.d(TAG, "Flagging one for removal " + uid + " " + itemUID);
                                    } else {
                                        // delete the entire group
                                        group.setGroupDeleted(true);
                                        Log.d(TAG, "Flagging group : " + group.getGroupID()
                                                + " for removal IDs: " + uid + " " + itemUID);
                                    }
                                }
                            }
                        }
                        synchronized (thread) {
                            thread.notify();
                        }
                        break;
                    default:
                        Log.w(TAG, "FalconOnMapEventListener other type " + mapEvent.getType());
                        break;
                }
            }
        }
    }

    private static class GCListener implements MapItem.OnGroupChangedListener {

        @Override
        public void onItemAdded(MapItem mapItem, MapGroup mapGroup) {
            Log.d(TAG, "Item " + mapItem.getUID() + " group: " + mapGroup.getFriendlyName() + " added");
        }

        /**
         * Completes removal of the Falcon group
         *
         * @param mapItem - the MapItem
         * @param mapGroup - the map group
         */
        @Override
        public void onItemRemoved(MapItem mapItem, MapGroup mapGroup) {

            /* commented out for now - can't figure out how to update a marker, have to delete and create
            String uid = mapItem.getUID();
            for (BioTracUnit unit: bioTracUnitList) {
                if (unit.getUID().equals(uid)) {
                    bioTracUnitList.remove(unit);
                    Log.d (TAG, "Item " + uid + " group: " + mapGroup.getFriendlyName()
                            + " deleted - unit list has " + bioTracUnitList.size() + " entries");
                    break;
                }
            } */
        }
    }

    /**
     * Flags all Falcon icons for deletion
     * @param view
     */
    public void deleteAllCallback (View view) {
        Iterator<FalconReadingGroup> groupIterator = groupList.iterator();
        while (groupIterator.hasNext()) {
            groupIterator.next().setGroupDeleted(true);
        }
        // wake the thread up
        synchronized (thread) {
            thread.notify();
        }
    }
}



